const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');

const app = express();
const port = 3000; // Change this to your desired port

// Middleware to parse JSON
app.use(bodyParser.json());

// MySQL Connection
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'contabilidad2',
});

connection.connect();

// Handle login request
app.post('/login', (req, res) => {
    const { username, password } = req.body;

    // Perform authentication logic using MySQL
    // Example: Select user from the database based on username and password
    const query = `SELECT * FROM usuarios WHERE usuario = ? AND contraseña = ?`;
    connection.query(query, [username, password], (err, results) => {
        if (err) {
            console.error('MySQL Error:', err);
            res.status(500).json({ success: false, message: 'Internal Server Error' });
            return;
        }

        if (results.length > 0) {
            res.json({ success: true, message: 'Login successful' });
        } else {
            res.json({ success: false, message: 'Invalid username or password' });
        }
    });
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});