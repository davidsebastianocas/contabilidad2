
function login() {
    var username = document.getElementById('correo').value;
    var password = document.getElementById('contraseña').value;

    // Send a request to your server for authentication
    // You may use AJAX, Fetch API, or other methods to send this data to your server.
    // Example using Fetch API:
    fetch('http://localhost:3000/index.html', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ usuario: username, contraseña: password }),
    })
    .then(response => response.json())
    .then(data => {
        // Handle the response from the server
        console.log(data);
    })
    .catch(error => {
        console.error('Error:', error);
    });



}

function redirec(){
    res.redirect("http://localhost:3000");
}

function listarProductos() {
    var nombre = document.getElementById('nombreProducto').value;
    var cantidad = document.getElementById('cantidadProducto').value;

    if (nombre && cantidad) {
        var listaProductos = document.getElementById('lista-productos');
        var nuevaFila = document.createElement('tr');
        nuevaFila.innerHTML = `<td>${nombre}</td><td>${cantidad}</td>`;
        listaProductos.appendChild(nuevaFila);

        document.getElementById('miFormulario').reset();
    } else {
        alert('Por favor, complete todos los campos del formulario.');
    }
}